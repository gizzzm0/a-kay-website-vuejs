import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from '@/layout/MainLayout'
import Home from '@/views/Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'ivu-menu-item-active',
  routes: [
    {
      path: '/',
      component: MainLayout,
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'HQ',
          component: Home
        }
      ]
    }
  ]
})
