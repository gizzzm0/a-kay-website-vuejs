import iView from 'iview'
import locale from 'iview/dist/locale/en-US'
import VueParticles from 'vue-particles'
import VueTypedJs from 'vue-typed-js'

import GlobalComponents from './globalComponents'

// Stylesheets
import 'iview/dist/styles/iview.css'

export default {
  install (Vue) {
    Vue.use(iView, { locale })
    Vue.use(VueParticles)
    Vue.use(VueTypedJs)
    Vue.use(GlobalComponents)
  }
}
