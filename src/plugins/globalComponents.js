import SectionHead from '../components/SectionHead'

const GlobalComponents = {
  install (Vue) {
    Vue.component('section-head', SectionHead)
  }
}

export default GlobalComponents
